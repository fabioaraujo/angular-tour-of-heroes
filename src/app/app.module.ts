import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { MyHeroesComponent } from './my-heroes/my-heroes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Hero } from './hero';
import { HeroService } from './hero.service';

import { AppRoutingModule } from './routing/routing.module'

@NgModule({
  declarations: [
    AppComponent,
    HeroDetailComponent,
    MyHeroesComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [HeroService],
  bootstrap: [AppComponent]
})
export class AppModule { }

